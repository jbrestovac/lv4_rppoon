﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treci__cetvrti_i_peti
{
    class Program
    {
        static void Main(string[] args)
        {
            //3. zadatak
            var rentableItems = new List<IRentable>();
            IRentable myBook = new Book("My new book");
            IRentable myVideo = new Video("My new video");
            rentableItems.Add(myBook);
            rentableItems.Add(myVideo);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.PrintTotalPrice(rentableItems);
            printer.DisplayItems(rentableItems);

            //4. zadatak
        
            IRentable myHotBook = new HotItem(new Book("My second book"));
            IRentable myHotVideo = new HotItem(new Video("My second video"));
            rentableItems.Add(myHotBook);
            rentableItems.Add(myHotVideo);
            printer.PrintTotalPrice(rentableItems);
            printer.DisplayItems(rentableItems);


            //5. zadatak:
            var flashSale = new List<IRentable>();
            foreach (IRentable item in rentableItems)
            {
                flashSale.Add(new DiscountedItem(item, 20));
            }
           
            printer.PrintTotalPrice(flashSale);
            printer.DisplayItems(flashSale);
        } 
    }
}
