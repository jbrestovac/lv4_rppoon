﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treci__cetvrti_i_peti
{ 
    //3. zadatak:
    class Book:IRentable
    {

        public String Name { get; private set; }
        private readonly double BaseBookPrice=3.99;
        public Book(String name)
        {
            this.Name = name;
        }
        public string Description { get { return this.Name; } 
        }
        public double CalculatePrice()
        {
            return BaseBookPrice;
        }
    }
}
