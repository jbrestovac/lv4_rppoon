﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treci__cetvrti_i_peti
{
    class DiscountedItem : RentableDecorator
    {
        private double discountPercentage = 0.0;
        public DiscountedItem(IRentable rentable, double disscountPercentage) : base(rentable)
        {
            this.discountPercentage = disscountPercentage;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice() * (this.discountPercentage / 100);
        }
        public override String Description
        {
            get
            {
                return base.Description + " now at " + this.discountPercentage + " % off!";
            }
        }
    }
}

