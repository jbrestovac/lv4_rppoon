﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treci__cetvrti_i_peti
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }

}
