﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prvi_zadatak
{
    class Program
    {
        //2. zadatak:
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\Jelena\\source\\repos\\LV4-RPPOON-BrestovacJ\\testfile");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] averagePerRow = adapter.CalculateAveragePerRow(dataset);
            double[] averagePerColumn = adapter.CalculateAveragePerColumn(dataset);
            Console.WriteLine("Row average: ");
            for (int i = 0; i < averagePerRow.Length; i++)
            {
                Console.WriteLine(averagePerRow[i]);
            }

            Console.WriteLine("Column average: ");
            for (int i = 0; i < averagePerColumn.Length; i++)
            {
                Console.WriteLine(averagePerColumn[i]);
            }
        }
    }
}
