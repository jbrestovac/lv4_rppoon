﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prvi_zadatak
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int rowCount = data.Length;
            int columnCount = data[0].Length;
            double[] results = new double[columnCount];
            double sum = 0.0;
            for (int i=0; i < columnCount; i++)
            {
                for(int j=0; j<rowCount; j++)
                {
                    sum += data[j][i];
                }
                results[i] = sum / rowCount;
                sum = 0.0;
            }
            return results;

        }
    }
}
